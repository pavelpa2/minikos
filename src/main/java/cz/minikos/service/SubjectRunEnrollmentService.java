package cz.minikos.service;

import cz.minikos.dao.StudentDao;
import cz.minikos.dao.SubjectRunEnrollmentDao;
import cz.minikos.model.ExamEnrollment;
import cz.minikos.model.Student;
import cz.minikos.model.SubjectRun;
import cz.minikos.model.SubjectRunEnrollment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class SubjectRunEnrollmentService {
    private final SubjectRunEnrollmentDao dao;
    private final StudentDao studentDao;
    private final TutorialParallelService tutorialParallelService;

    @Autowired
    public SubjectRunEnrollmentService(SubjectRunEnrollmentDao dao, StudentDao studentDao, TutorialParallelService tutorialParallelService) {
        this.dao = dao;
        this.studentDao = studentDao;
        this.tutorialParallelService = tutorialParallelService;
    }

    @Transactional
    public void delete(SubjectRunEnrollment subjectRunEnrollment){
        dao.remove(subjectRunEnrollment);
    }
    @Transactional
    public SubjectRunEnrollment create(SubjectRun subjectRun) {
        Objects.requireNonNull(subjectRun);
        SubjectRunEnrollment subjectRunEnrollment = new SubjectRunEnrollment();
        subjectRunEnrollment.setSubjectRun(subjectRun);
        subjectRunEnrollment.setTutorialParallel(subjectRun.getTutorialParallel());
        dao.persist(subjectRunEnrollment);
        return  subjectRunEnrollment;
    }

    public SubjectRunEnrollment find(Integer id){
        return dao.find(id);
    }
}
