package cz.minikos.service;

import cz.minikos.dao.*;
import cz.minikos.exception.*;
import cz.minikos.model.*;
import cz.minikos.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class TeacherService {
    private final TeacherDao dao;
    private final TutorialParallelDao tutorialParallelDao;
    private final SubjectRunEnrollmentDao subjectRunEnrollmentDao;
    private final ExamTermDao examTermDao;
    private final SubjectDao subjectDao;
    private final GradeDao gradeDao;
    private final SemesterService semesterService;
    private final Semester currentSemester;

    @Autowired
    public TeacherService(SemesterService semesterService, TeacherDao dao, TutorialParallelDao tutorialParallelDao, SubjectRunEnrollmentDao subjectRunEnrollmentDao, ExamTermDao examTermDao, SubjectDao subjectDao, GradeDao gradeDao) {
        this.dao = dao;
        this.tutorialParallelDao = tutorialParallelDao;
        this.subjectRunEnrollmentDao = subjectRunEnrollmentDao;
        this.examTermDao = examTermDao;
        this.subjectDao = subjectDao;
        this.gradeDao = gradeDao;
        this.semesterService = semesterService;
        this.currentSemester = semesterService.find(Constants.CURRENT_SEMESTER_NAME);
    }

    public Teacher find(Integer id){
        return dao.find(id);
    }

    @Transactional
    public void giveAssessmentToStudent(Teacher teacher, Student student, SubjectRunEnrollment subjectRunEnrollment) {
        Objects.requireNonNull(teacher);
        Objects.requireNonNull(student);
        Objects.requireNonNull(subjectRunEnrollment);
        Objects.requireNonNull(student.getSubjectRunEnrollments());
        Objects.requireNonNull(subjectRunEnrollment.getTutorialParallel());
        Objects.requireNonNull(subjectRunEnrollment.getTutorialParallel().getTutor());

        //  check that student is really enrolled
        boolean studentIsEnrolled = student.getSubjectRunEnrollments().contains(subjectRunEnrollment);
        if (!studentIsEnrolled) {throw  new StudentIsNotEnrolled("Student is not enrolled to that subject");}
        Teacher tutorOfTutorialParallel = subjectRunEnrollment.getTutorialParallel().getTutor();
        if (!tutorOfTutorialParallel.equals(teacher)) {
            throw new TeacherCantGiveAssessment("Teacher is not lecturor of tutorial parallel");
        }

        subjectRunEnrollment
                .setAssessmentGiven(true)
                .setAssessmentGiver(teacher)
                .setAssessmentGivenAt(LocalDateTime.now());
        subjectRunEnrollmentDao.update(subjectRunEnrollment);
    }

    @Transactional
    public void giveAssessmentToStudent(Teacher teacher, Student student, SubjectRun subjectRun) {
        SubjectRunEnrollment subjectRunEnrollment = student.getSubjectRunEnrollmentOfSubjectRun(subjectRun, currentSemester);
        if (subjectRunEnrollment == null){
            throw new StudentIsNotEnrolled("Student is not enrolled to that");
        }
        giveAssessmentToStudent(teacher, student, subjectRunEnrollment);
    }
    @Transactional
    public void giveAssessmentToStudent(Integer idTeacher, Student student, SubjectRunEnrollment subjectRunEnrollment){
        Teacher t = dao.find(idTeacher);
        NotFoundException.checkNullThrowNotFound(t, "Teacher", idTeacher);
        giveAssessmentToStudent(t, student, subjectRunEnrollment);
    }

    @Transactional
    public void giveGradeToStudent(Teacher teacher, Student student, ExamEnrollment examEnrollment, Grade.GradeEnum gradeEnum) {
        Integer idOfExamCreator = examEnrollment.getExamTerm().getLecturer().getId();
        Integer idOfTeacher = teacher.getId();
        if (!idOfExamCreator.equals(idOfTeacher)){
            throw new TeacherIsNotCreatorOfExamException("This teacher is not a creator of this exam");
        }
        boolean studentIsEnrolled = student.getExamEnrollments().contains(examEnrollment);
        if (!studentIsEnrolled) {
            throw new EnrollmentException("Student doesn't enroll this exam");
        }
            examEnrollment.getGrade()
                    .setGrade(gradeEnum)
                    .setGradeGiver(teacher)
                    .setGradeGivenAt(LocalDateTime.now());
            gradeDao.persist(examEnrollment.getGrade());
    }
}
