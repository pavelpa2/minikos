package cz.minikos.service;

import cz.minikos.dao.SubjectDao;
import cz.minikos.dao.SubjectRunDao;
import cz.minikos.dao.TutorialParallelDao;
import cz.minikos.exception.MiniKOSException;
import cz.minikos.exception.NotFoundException;
import cz.minikos.exception.SubjectRunException;
import cz.minikos.exception.TutorNotInSubjectRunException;
import cz.minikos.model.*;
import cz.minikos.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

@Service
public class SubjectRunService {
    private final SubjectRunDao dao;
    private final SubjectDao subjectDao;
    private final TutorialParallelDao tutorialParallelDao;
    private final Semester currentSemester;
    private final SemesterService semesterService;

    @Autowired
    public SubjectRunService(SubjectRunDao dao, SubjectDao subjectDao, TutorialParallelDao tutorialParallelDao, Semester currentSemester, SemesterService semesterService1) {
        this.dao = dao;
        this.subjectDao = subjectDao;
        this.tutorialParallelDao = tutorialParallelDao;
        this.currentSemester = currentSemester;
        this.semesterService = semesterService1;
    }

    @Transactional
    public SubjectRun createSubjectRun(String name, String code, Semester semester, int credits) {
        Subject subject = subjectDao.find(name, code);
        if (subject == null) {
            throw new MiniKOSException("Cannot create subject run of not existing subject");
        }
        return createSubjectRun(subject, semester, credits);
    }

    public SubjectRun createSubjectRun(Integer idSubject, Semester semester, int credits){
        Subject subject = subjectDao.find(idSubject);
        if (subject == null) {
            throw new MiniKOSException("Cannot create subject run of not existing subject");
        }
        return createSubjectRun(subject, semester, credits);
    }

    @Transactional
    public SubjectRun createSubjectRunInThisSemester(Integer idSubject, int credits){
        Subject subject = subjectDao.find(idSubject);
        Semester semester = semesterService.find(Constants.CURRENT_SEMESTER_NAME);
        if (semester == null){
            semester = semesterService.create(Constants.CURRENT_SEMESTER_NAME);
        }
        if (subject == null) {
            throw new MiniKOSException("Cannot create subject run of not existing subject");
        }
        return createSubjectRun(subject, semester, credits);
    }

    @Transactional
    public SubjectRun createSubjectRun(Subject subject, Semester semester, int credits) {
        SubjectRun subjectRun;
        subjectRun = getSubjectRunInSemester(subject.getName(), subject.getCode(), semester);
        if (subjectRun != null) {
            return subjectRun;
        }
        Semester semesterCheck = semesterService.find(semester.getName());
        if (semesterCheck == null){
            throw new MiniKOSException("Semester doesnt exist!");
        }
        subjectRun = new SubjectRun();
        subjectRun.setSubject(subject);
        subjectRun.setSemester(semesterCheck);
        subjectRun.setCredits(credits);
        dao.persist(subjectRun);
        return subjectRun;
    }

    public SubjectRun getSubjectRunInSemester(String name, String code, Semester semester) {
        SubjectRun subjectRunExistingInSemester = dao.find(name, code, semester);
        return subjectRunExistingInSemester;
    }

    public List<SubjectRun> getListedSubjectRunsInSemester(Semester semester) {
        return dao.findAll(semester);
    }

    public List<SubjectRun> getListedSubjectRunsCurrentSemester() {
        Semester currentSemester = semesterService.find(Constants.CURRENT_SEMESTER_NAME);
        return dao.findAll(currentSemester);
    }

    @Transactional
    public void addTutor(SubjectRun subjectRun, Teacher tutor) {
        Objects.requireNonNull(subjectRun);
        Objects.requireNonNull(tutor);
        subjectRun.addTutor(tutor);
        dao.update(subjectRun);
    }

    @Transactional
    public void addLecturer(SubjectRun subjectRun, Teacher lecturer) {
        Objects.requireNonNull(subjectRun);
        Objects.requireNonNull(lecturer);
        subjectRun.addLecturer(lecturer);
        dao.update(subjectRun);
    }

    /**
     * Add TutorialParallel to SubjectRun.
     * SubjectRun can have at max 1 Tutorial Parallel.
     * Requirement: Tutor of TutorialParallel has to be a Tutor of SubjectRun
     * If SubjectRun already has TutorialParallel this will throw exception.
     */
    @Transactional
    public void addTutorialParallel(SubjectRun subjectRun, TutorialParallel tutorialParallel) {
        //  check if subjectRun already has tutorialParallel
        if (subjectRun.getTutorialParallel() != null) {
            throw new SubjectRunException("SubjectRun already has a tutorialParallel");
        }
        isTeacherTutorOfSubjectRun(subjectRun, tutorialParallel.getTutor()); // throws exception if not
        subjectRun.setTutorialParallel(tutorialParallel);
        dao.update(subjectRun);
    }

    private void isTeacherTutorOfSubjectRun(SubjectRun subjectRun, Teacher tutor) {
        boolean tutorIsTutorOfSubjectRun = subjectRun.getTutors().contains(tutor);
        if (!tutorIsTutorOfSubjectRun) {
            throw new TutorNotInSubjectRunException("Tutor of tutorial parallel is not a tutor of SubjectRun");
        }
    }

    @Transactional
    public void updateTutorialParallel(SubjectRun subjectRun, TutorialParallel tutorialParallel) {
        updateTutorialParallel(subjectRun, tutorialParallel.getDay(), tutorialParallel.getTutor(), tutorialParallel.getTime());
    }

    @Transactional
    public void updateTutorialParallel(SubjectRun subjectRun, String day, Teacher tutor, LocalTime time) {
        isTeacherTutorOfSubjectRun(subjectRun, tutor); // throws exception if not
        TutorialParallel currentTutorialParallel = subjectRun.getTutorialParallel();
        currentTutorialParallel.setTutor(tutor);
        currentTutorialParallel.setDay(day);
        currentTutorialParallel.setTime(time);
        tutorialParallelDao.update(currentTutorialParallel);
    }

    public TutorialParallel getTutorialParallelOfSubjectRun(Integer idSubjectRun){
        SubjectRun subjectRun = dao.find(idSubjectRun);
        if (subjectRun == null){
            throw NotFoundException.create("SubjectRun", idSubjectRun);
        }
        return subjectRun.getTutorialParallel();
    }

    public SubjectRun find(Integer id){
        return dao.find(id);
    }


    public SubjectRun getSubjectRun(Integer id){
        return dao.find(id);
    }
}
