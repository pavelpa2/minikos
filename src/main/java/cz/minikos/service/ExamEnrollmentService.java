package cz.minikos.service;

import cz.minikos.dao.ExamEnrollmentDao;
import cz.minikos.dao.StudentDao;
import cz.minikos.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
public class ExamEnrollmentService {
    private final ExamEnrollmentDao dao;
    private final StudentDao studentDao;
    private final GradeService gradeService;

    @Autowired
    public ExamEnrollmentService(ExamEnrollmentDao dao, StudentDao studentDao, GradeService gradeService) {
        this.dao = dao;
        this.studentDao = studentDao;
        this.gradeService = gradeService;
    }

    public ExamEnrollment create(ExamTerm examTerm) {
        Objects.requireNonNull(examTerm);
        ExamEnrollment examEnrollment = new ExamEnrollment();
        examEnrollment.setExamTerm(examTerm);
        Grade grade = gradeService.create();
        examEnrollment.setGrade(grade);
        dao.persist(examEnrollment);
        return examEnrollment;
    }
}
