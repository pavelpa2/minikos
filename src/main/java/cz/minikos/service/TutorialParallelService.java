package cz.minikos.service;

import cz.minikos.dao.TutorialParallelDao;
import cz.minikos.model.Teacher;
import cz.minikos.model.TutorialParallel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.Objects;

@Service
public class TutorialParallelService {
    private final TutorialParallelDao dao;

    @Autowired
    public TutorialParallelService(TutorialParallelDao dao) {
        this.dao = dao;
    }

    @Transactional
    public TutorialParallel createTutorialParallel(String day, LocalTime time, Teacher tutor) {
        Objects.requireNonNull(tutor);
        Objects.requireNonNull(time);
        Objects.requireNonNull(day);
        TutorialParallel tutorialParallel = new TutorialParallel();
        tutorialParallel.setDay(day);
        tutorialParallel.setTime(time);
        tutorialParallel.setTutor(tutor);
        dao.persist(tutorialParallel);
        return tutorialParallel;
    }

}
