package cz.minikos.service;

import cz.minikos.dao.ExamEnrollmentDao;
import cz.minikos.dao.ExamTermDao;
import cz.minikos.dao.StudentDao;
import cz.minikos.exception.*;
import cz.minikos.model.*;
import cz.minikos.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentDao dao;
    private final SubjectRunEnrollmentService subjectRunEnrollmentService;
    private final ExamEnrollmentService examEnrollmentService;
    private final ExamTermDao examTermDao;
    private final ExamEnrollmentDao examEnrollmentDao;
    private final SemesterService semesterDao;
    private final Semester currentSemester;

    @Autowired
    public StudentService(StudentDao studentDao, SubjectRunEnrollmentService subjectRunEnrollmentService, ExamEnrollmentService examEnrollmentService, Semester currentSemester, ExamTermDao examTermDao, ExamEnrollmentDao examEnrollmentDao, SemesterService semesterDao) {
        this.dao = studentDao;
        this.subjectRunEnrollmentService = subjectRunEnrollmentService;
        this.examEnrollmentService = examEnrollmentService;
        this.examTermDao = examTermDao;
        this.examEnrollmentDao = examEnrollmentDao;
        this.semesterDao = semesterDao;
        this.currentSemester = semesterDao.find(Constants.CURRENT_SEMESTER_NAME);
    }

    @Transactional(readOnly = true)
    public List<Student> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public Student find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public List<SubjectRun> getEnrolledSubjectRuns(Integer studentId){
        Student student = dao.find(studentId);
        NotFoundException.checkNullThrowNotFound(student, "Student", studentId);
        return student.getEnrolledSubjectRuns(this.currentSemester);
    }

    @Transactional
    public List<SubjectRunEnrollment> getSubjectRunEnrollments(Integer studentId){
        Student student = dao.find(studentId);
        NotFoundException.checkNullThrowNotFound(student, "Student", studentId);
        return student.getSubjectRunEnrollments(this.currentSemester);
    }

    @Transactional
    public SubjectRunEnrollment enrollSubject(Integer studentId, SubjectRun subjectRun) {
        Student student = dao.find(studentId);
        if (student.getSubjectRunEnrollments() == null) {
            student.setSubjectRunEnrollments(new ArrayList<>());
        } else {
            boolean studentIsAlreadyEnrolled = student.isStudentEnrolledToSubjectAtSemester(subjectRun.getSubject(), Constants.CURRENT_SEMESTER_NAME);
            if (studentIsAlreadyEnrolled) {
                throw new StudentAlreadyEnrolledToSubjectRun("Student is already enrolled to subject this semester");
            }

            boolean studentIsAbleToEnroll = student.getHowManyTimesStudentWasEnrolledInASubject(subjectRun.getSubject()) < 2;
            if (!studentIsAbleToEnroll) {
                throw new StudentCannotEnrollToSubjectRunAgain("Student is not able to enroll to a subject, because he already enrolled 2 times in the past.");
            }
        }
        SubjectRunEnrollment subjectRunEnrollment = subjectRunEnrollmentService.create(subjectRun);
        student.getSubjectRunEnrollments().add(subjectRunEnrollment);
        dao.update(student);
        return subjectRunEnrollment;
    }


    @Transactional
    public SubjectRunEnrollment enrollSubject(Student student, SubjectRun subjectRun) {
        Objects.requireNonNull(student);
        if (student.getSubjectRunEnrollments() == null) {
            student.setSubjectRunEnrollments(new ArrayList<>());
        } else {
            boolean studentIsAlreadyEnrolled = student.isStudentEnrolledToSubjectAtSemester(subjectRun.getSubject(), Constants.CURRENT_SEMESTER_NAME);
            if (studentIsAlreadyEnrolled) {
                throw new StudentAlreadyEnrolledToSubjectRun("Student is already enrolled to subject this semester");
            }

            boolean studentIsAbleToEnroll = student.getHowManyTimesStudentWasEnrolledInASubject(subjectRun.getSubject()) < 2;
            if (!studentIsAbleToEnroll) {
                throw new StudentCannotEnrollToSubjectRunAgain("Student is not able to enroll to a subject, because he already enrolled 2 times in the past.");
            }
        }
        SubjectRunEnrollment subjectRunEnrollment = subjectRunEnrollmentService.create(subjectRun);
        student.getSubjectRunEnrollments().add(subjectRunEnrollment);
        dao.update(student);
        return subjectRunEnrollment;
    }

    @Transactional
    public void cancelSubjectEnrollment(Integer studentId, SubjectRun subjectRun){
        Student student = dao.find(studentId);
        NotFoundException.checkNullThrowNotFound(student, "Student", studentId);
        if (!student.isStudentEnrolledToSubjectAtSemester(subjectRun.getSubject(), Constants.CURRENT_SEMESTER_NAME)) {
            throw new StudentIsNotEnrolled("Student is not enrolled in the first place.");
        }
        List<SubjectRunEnrollment> subjectRunEnrollmentsList = student.getSubjectRunEnrollments();
        Optional<SubjectRunEnrollment> subjectRunEnrollmentOptional = subjectRunEnrollmentsList.stream().filter(e->e.getSubjectRun() == subjectRun).findAny();
        if (subjectRunEnrollmentOptional.isPresent()){
            SubjectRunEnrollment subjectRunEnrollment = subjectRunEnrollmentOptional.get();
            student.getSubjectRunEnrollments().remove(subjectRunEnrollment);
            subjectRunEnrollmentService.delete(subjectRunEnrollment);
            dao.update(student);
        }
    }

    public int getNumberOfEnrolledCreditsCurrentSemester(Student student) {
        return student.getNumberOfEnrolledCredits(currentSemester);
    }

    @Transactional
    public ExamEnrollment enrollExamTerm(Integer studentId, ExamTerm examTerm) {
        Student student = dao.find(studentId);
        NotFoundException.checkNullThrowNotFound(student, "Student", studentId);
        return enrollExamTerm(student, examTerm);
    }

    @Transactional
    public ExamEnrollment enrollExamTerm(Student student, ExamTerm examTerm) {
        if (student.getExamEnrollments() == null) {
            student.setExamEnrollments(new ArrayList<>());
        }
        if(student.doesStudentHaveActiveExamEnrollment(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME)){
            throw new EnrollmentException("Student can't enroll this exam, because student is already enrolled to exam term of subject " + examTerm.getSubjectRun().getSubject().getName());
        }
        boolean studentIsEnrolledToSubjectRun = student.isStudentEnrolledToSubjectAtSemester(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME);
        if (!studentIsEnrolledToSubjectRun) {
            throw new EnrollmentException("Student didn't enroll in a subject: " + examTerm.getSubjectRun().getSubject().getName() + " this semester: " + currentSemester);
        }

        int howManyTimesStudentsWasEnrolledInAnExamAtSemester = student.getHowManyTimesStudentWasEnrolledInAExamAtSemester(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME);
        boolean studentAlreadyHasAssessment = student.didStudentReceiveAssessmentFromSubjectRun(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME);
        if (!studentAlreadyHasAssessment){
            throw new EnrollmentException("Student hasnt received assessment yet!");
        }
        if (howManyTimesStudentsWasEnrolledInAnExamAtSemester < 2) {
            ExamEnrollment examEnrollment = examEnrollmentService.create(examTerm);
            student.getExamEnrollments().add(examEnrollment);
            dao.update(student);
            return examEnrollment;
        }
        if (howManyTimesStudentsWasEnrolledInAnExamAtSemester == 2) {
            throw new EnrollmentException("Student can't enroll this exam, because student already has 2 tries, use enrollExtraExamTerm");
        }
        if (howManyTimesStudentsWasEnrolledInAnExamAtSemester == 3) {
            throw new EnrollmentException("Student can't enroll in a exam of this subject: " + examTerm.getSubjectRun().getSubject().getName() +
                    " this semester: " + currentSemester + " , because student has already enrolled it 3 times.");
        }
        return null;
    }

    @Transactional
    public ExamEnrollment enrollExtraExamTerm(Integer studentId, ExamTerm examTerm) {
        Student student = dao.find(studentId);
        return this.enrollExtraExamTerm(student, examTerm);
    }

    @Transactional
    public ExamEnrollment enrollExtraExamTerm(Student student, ExamTerm examTerm) {
        if(student.doesStudentHaveActiveExamEnrollment(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME)){
            throw new EnrollmentException("Student can't enroll this exam, because student already has this exam enrolled another term");
        }
        boolean studentIsEnrolled = student.isStudentEnrolledToSubjectAtSemester(examTerm.getSubjectRun().getSubject(),Constants.CURRENT_SEMESTER_NAME);
        if (!studentIsEnrolled) {
            throw new EnrollmentException("Student didn't enroll in a subject: " + examTerm.getSubjectRun().getSubject().getName() + " this semester: " + currentSemester);
        }
        if (student.getHowManyTimesStudentWasEnrolledInAExamAtSemester(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME) >= 3) {
            throw new EnrollmentException("Student cannot enroll to exam term from subject anymore");
        }
        if (student.getHowManyTimesStudentWasEnrolledInAExamAtSemester(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME) < 2){
            throw new EnrollmentException("Cant enroll to extra term because normal term is available");
        }
        if (student.getExtraExamEnrollmentsAvailable() <= 0) {
            throw new EnrollmentException("Student can't enroll in a exam of this subject: " + examTerm.getSubjectRun().getSubject().getName() +
                    "because he doesn't have extra exam enrollments tokens.");
        }
        student.setExtraExamEnrollmentsAvailable(student.getExtraExamEnrollmentsAvailable() - 1);
        ExamEnrollment examEnrollment = examEnrollmentService.create(examTerm);
        student.getExamEnrollments().add(examEnrollment);
        dao.update(student);
        return examEnrollment;
    }


    public ExamEnrollment getLastGradedExamEnrollment(Student student, Subject subject) {
        return student.getLastGradedExamEnrollmentFromSubjectAtSemester(subject, Constants.CURRENT_SEMESTER_NAME);
    }

//    @Transactional
//    public void cancelExamEnrollment(Student student, Subject subject) {
//        student.cancelExamEnrollment(subject, currentSemester);
//        dao.update(student);
//    }
//
    @Transactional
    public void cancelExamEnrollment(Student student, ExamTerm examTerm) {
        student.cancelExamEnrollment(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME);
        dao.update(student);
    }

    @Transactional
    public void cancelExamEnrollment(Integer idStudent, ExamTerm examTerm) {
        Student student = dao.find(idStudent);
        NotFoundException.checkNullThrowNotFound(student, "Student", idStudent);
        ExamEnrollment examEnrollment = student.cancelExtraExamEnrollment(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME);
        if (student.getHowManyTimesStudentWasEnrolledInAExamAtSemester(examTerm.getSubjectRun().getSubject(), Constants.CURRENT_SEMESTER_NAME) >= 3) {
            //  cancelling an extra exam term enrollment
            student.setExtraExamEnrollmentsAvailable(student.getExtraExamEnrollmentsAvailable() + 1);
        }
        //  cancelling regular exam term enrollment
        examEnrollmentDao.remove(examEnrollment) ;
        dao.update(student);
    }
}
