package cz.minikos.service;

import cz.minikos.dao.SemesterDao;
import cz.minikos.model.Semester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SemesterService {
    private final SemesterDao semesterDao;

    @Autowired
    public SemesterService(SemesterDao semesterDao) {
        this.semesterDao = semesterDao;
    }

    public Semester find(String name){
        return semesterDao.find(name);
    }

    public Semester find(Integer id){
        return semesterDao.find(id);
    }

    public Semester create(String name){
        Semester semester = new Semester();
        semester.setName(name);
        semesterDao.persist(semester);
        return semester;
    }
}
