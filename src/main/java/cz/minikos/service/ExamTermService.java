package cz.minikos.service;

import cz.minikos.dao.ExamEnrollmentDao;
import cz.minikos.dao.ExamTermDao;
import cz.minikos.model.ExamEnrollment;
import cz.minikos.model.ExamTerm;
import cz.minikos.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@Service
public class ExamTermService {
    private final ExamTermDao dao;
    private final ExamEnrollmentDao examEnrollmentDao;

    @Autowired
    public ExamTermService(ExamTermDao dao, ExamEnrollmentDao examEnrollmentDao) {
        this.dao = dao;
        this.examEnrollmentDao = examEnrollmentDao;
    }

    public ExamTerm find(Integer id){
        return dao.find(id);
    }



}
