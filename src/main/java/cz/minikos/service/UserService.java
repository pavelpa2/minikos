package cz.minikos.service;

import cz.minikos.dao.UserDao;
import cz.minikos.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {
    private final UserDao userDao;
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(UserDao userDao){
        this.userDao = userDao;
    }

    public User find(String username){
        return userDao.findByUsername(username);
    }

   @PostAuthorize("hasRole('ROLE_ADMIN') or (returnObject.username  == principal.username)")
    public User find(Integer id){
        return userDao.find(id);
    }

    public List<User> findAll(){
        return userDao.findAll();
    }

    @Transactional
    public void create(User user){
        userDao.persist(user);
    }
}
