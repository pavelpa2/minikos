package cz.minikos.service;

import cz.minikos.dao.ExamEnrollmentDao;
import cz.minikos.dao.ExamTermDao;
import cz.minikos.dao.SubjectDao;
import cz.minikos.dao.SubjectRunEnrollmentDao;
import cz.minikos.exception.MiniKOSException;
import cz.minikos.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SubjectService {
    private final SubjectDao dao;
    private final SubjectRunEnrollmentDao subjectRunEnrollmentDao;
    private final ExamEnrollmentDao examEnrollmentDao;
    private final ExamTermDao examTermDao;

    @Autowired
    public SubjectService(SubjectDao dao, SubjectRunEnrollmentDao subjectRunEnrollmentDao, ExamEnrollmentDao examEnrollmentDao, ExamTermDao examTermDao) {
        this.dao = dao;
        this.subjectRunEnrollmentDao = subjectRunEnrollmentDao;
        this.examEnrollmentDao = examEnrollmentDao;
        this.examTermDao = examTermDao;
    }

    @Transactional
    public Subject createSubject(String name, String code) {
        Subject alreadyExistingSubject = dao.find(name, code);
        if (alreadyExistingSubject != null) {
            throw new MiniKOSException("Subject already exists");
        }
        Subject subject = new Subject();
        subject.setName(name);
        subject.setCode(code);
        dao.persist(subject);
        return subject;
    }

    public Subject find(Integer id){
        return dao.find(id);
    }
}
