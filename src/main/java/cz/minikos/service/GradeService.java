package cz.minikos.service;

import cz.minikos.dao.GradeDao;
import cz.minikos.model.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GradeService {
    private final GradeDao dao;

    @Autowired
    public GradeService(GradeDao dao) {
        this.dao = dao;
    }

    public Grade create() {
        Grade grade = new Grade();
        dao.persist(grade);
        return grade;
    }
}