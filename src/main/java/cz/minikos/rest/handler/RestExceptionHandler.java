package cz.minikos.rest.handler;

import cz.minikos.exception.*;
import cz.minikos.security.SecurityUtils;
import org.eclipse.persistence.exceptions.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;

/**
 * Exception handlers for REST controllers.
 * <p>
 * The general pattern should be that unless an exception can be handled in a more appropriate place it bubbles up to a
 * REST controller which originally received the request. There, it is caught by this handler, logged and a reasonable
 * error message is returned to the user.
 */
@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    private static void logException(RuntimeException ex) {
        LOG.error("Exception caught:", ex);
    }

    private static ErrorInfo errorInfo(HttpServletRequest request, Throwable e) {
        return new ErrorInfo(e.getMessage(), request.getRequestURI());
    }

    @ExceptionHandler(PersistenceException.class)
    public ResponseEntity<ErrorInfo> persistenceException(HttpServletRequest request, PersistenceException e) {
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e.getCause()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> resourceNotFound(HttpServletRequest request, NotFoundException e) {
        // Not necessary to log NotFoundException, they may be quite frequent and do not represent an issue with the application
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ErrorInfo> validation(HttpServletRequest request, ValidationException e) {
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ErrorInfo> accessDenied(HttpServletRequest request, AccessDeniedException e) {
        // Spring Boot throws Access Denied when trying to access a secured method with anonymous authentication token
        // We want to let such exception out, so that it is handled by the authentication entry point (which returns 401)
        if (SecurityUtils.isAuthenticatedAnonymously()) {
            throw e;
        }
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(StudentAlreadyEnrolledToSubjectRun.class)
    public ResponseEntity<ErrorInfo> studentAlreadyEnrolled(HttpServletRequest request, StudentAlreadyEnrolledToSubjectRun e) {
        //  because he is already enrolled this semester!
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(StudentCannotEnrollToSubjectRunAgain.class)
    public ResponseEntity<ErrorInfo> studentCannotEnrollAgain(HttpServletRequest request, StudentCannotEnrollToSubjectRunAgain e) {
        //  1) because he already enrolled two times in the past
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(TeacherCantGiveAssessment.class)
    public ResponseEntity<ErrorInfo> teacherCantGiveAssessment(HttpServletRequest request, TeacherCantGiveAssessment e) {
        //  1) because he isnt lecturer of tutorial parallel
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(TeacherCantGradeStudent.class)
    public ResponseEntity<ErrorInfo> teacherCantGradeStudent(HttpServletRequest request, TeacherCantGradeStudent e) {
        //  1) teacher is not lecturor that created exam term
        //  2) student didnt receive assessment (zapocet)
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(StudentCannotEnrollToExamTerm.class)
    public ResponseEntity<ErrorInfo> studentCannotEnrollToExamTerm(HttpServletRequest request, StudentCannotEnrollToExamTerm e) {
        //  1) he didnt receive assessment (zapocet)
        //  2) he already enrolled and failed 2 exam terms
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(StudentCannotCancelExamEnrollment.class)
    public ResponseEntity<ErrorInfo> studentCannotCancelExamEnrollment(HttpServletRequest request, StudentCannotCancelExamEnrollment e) {
        //  1) because exam is already due
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(StudentIsNotEnrolled.class)
    public ResponseEntity<ErrorInfo> studentIsNotEnrolled(HttpServletRequest request, StudentIsNotEnrolled e) {
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(NotLoggedIn.class)
    public ResponseEntity<ErrorInfo> notLoggedIn(HttpServletRequest request, NotLoggedIn e) {
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(EnrollmentException.class)
    public ResponseEntity<ErrorInfo> enrollmentException(HttpServletRequest request, EnrollmentException e) {
        logException(e);
        return new ResponseEntity<>(errorInfo(request, e), HttpStatus.BAD_REQUEST);
    }
}
