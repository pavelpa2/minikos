package cz.minikos.rest;

import cz.minikos.exception.NotFoundException;
import cz.minikos.exception.UserAlreadyRegistered;
import cz.minikos.model.User;
import cz.minikos.rest.util.RestUtils;
import cz.minikos.security.DefaultAuthenticationProvider;
import cz.minikos.security.SecurityUtils;
import cz.minikos.security.model.UserDetails;
import cz.minikos.service.UserService;
import cz.minikos.service.security.UserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/user")
public class UserController {
    private final UserService userService;
    private final DefaultAuthenticationProvider defaultAuthenticationProvider;
    private final UserDetailsService userDetailsService;
    private final PasswordEncoder passwordEncoder;
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService, DefaultAuthenticationProvider defaultAuthenticationProvider, PasswordEncoder passwordEncoder, UserDetailsService userDetailsService){
        this.userDetailsService = userDetailsService;
        this.userService = userService;
        this.defaultAuthenticationProvider = defaultAuthenticationProvider;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> getAllUsers(){
        List<User> res = userService.findAll();
        return res;
    }

    @GetMapping(value = "/{id}")
    public User getUser(@PathVariable Integer id){
        if (SecurityUtils.isAuthenticatedAnonymously()){
            throw new NotFoundException("");
        }
        User user = userService.find(id);
        NotFoundException.checkNullThrowNotFound(user, "User", id);
        return user;
    }
}