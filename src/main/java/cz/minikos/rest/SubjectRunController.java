package cz.minikos.rest;

import cz.minikos.exception.NotFoundException;
import cz.minikos.model.*;
import cz.minikos.security.SecurityUtils;
import cz.minikos.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/subjectRun")
public class SubjectRunController {
    private final SubjectRunService subjectRunService;
    private final StudentService studentService;
    private final SubjectService subjectService;
    private final SemesterService semesterService;
    private final TeacherService teacherService;

    @Autowired
    public SubjectRunController(StudentService studentService, SubjectRunService subjectRunService, SubjectService subjectService, TeacherService teacherService, SemesterService semesterService, TeacherService teacherService1) {
        this.studentService = studentService;
        this.subjectRunService = subjectRunService;
        this.subjectService = subjectService;
        this.semesterService = semesterService;
        this.teacherService = teacherService1;
    }

    @GetMapping()
    public List<SubjectRun> getSubjectRuns(){
        System.out.println("this got called");
        return subjectRunService.getListedSubjectRunsCurrentSemester();
    }

    @GetMapping(value = "/{id}")
    public SubjectRun getSubjectRun(@PathVariable Integer id){
        return subjectRunService.find(id);
    }

    @GetMapping(value = "/{id}/tutorialParallel", produces = MediaType.APPLICATION_JSON_VALUE)
    public TutorialParallel getTutorialParallelOfSubjectRun(@PathVariable Integer id) {
        TutorialParallel tutorialParallel;
        try{
            tutorialParallel = subjectRunService.getTutorialParallelOfSubjectRun(id);
        }catch (NotFoundException e){
            throw e;
        }
        return tutorialParallel;
    }

    @PostMapping(value = "/enroll", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public SubjectRunEnrollment enrollToSubjectRun(@RequestBody SubjectRun subjectRunParam){
        Integer idSubjectRun = subjectRunParam.getId();
        Student student = (Student) SecurityUtils.getCurrentUser();
        SubjectRun subjectRun = subjectRunService.getSubjectRun(idSubjectRun);
        NotFoundException.checkNullThrowNotFound(subjectRun, "SubjectRun", idSubjectRun);
        return studentService.enrollSubject(student.getId(), subjectRun);
    }

    @DeleteMapping(value = "/cancelEnrollment/{idSubjectRun}")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public void cancelEnrollment(@PathVariable Integer idSubjectRun){
        Student student = (Student) SecurityUtils.getCurrentUser();
        SubjectRun s = subjectRunService.find(idSubjectRun);
        NotFoundException.checkNullThrowNotFound(s, "SubjectRun", idSubjectRun);
        studentService.cancelSubjectEnrollment(student.getId(), s);
    }

    @PostMapping(value = "/{idSubject}/{credits}/create")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public SubjectRun createSubjectRun(@PathVariable Integer idSubject, @PathVariable Integer credits){
        Subject subject = subjectService.find(idSubject);
        NotFoundException.checkNullThrowNotFound(subject, "Subject", idSubject);
        return subjectRunService.createSubjectRunInThisSemester(idSubject, credits);
    }

    @PostMapping(value = "/{idSubject}/{credits}/{semesterName}/create")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public SubjectRun createSubjectRun(@PathVariable Integer idSubject, @PathVariable Integer credits, @PathVariable String semesterName){
        Subject subject = subjectService.find(idSubject);
        Semester semester = semesterService.find(semesterName);
        NotFoundException.checkNullThrowNotFound(semester, "Semester", semester.getName());
        NotFoundException.checkNullThrowNotFound(subject, "Subject", idSubject);
        return subjectRunService.createSubjectRun(idSubject, semester, credits);
    }

    @PutMapping(value = "/{idSubjectRun}/{idStudent}/{idTeacher}/giveAssessment")
    public void giveAssessmentToStudent(@PathVariable Integer idSubjectRun, @PathVariable Integer idStudent, @PathVariable Integer idTeacher){
        Student student = studentService.find(idStudent);
        Teacher teacher = teacherService.find(idTeacher);
        SubjectRun subject = subjectRunService.find(idSubjectRun);
        NotFoundException.checkNullThrowNotFound(student, "Student", idStudent);
        NotFoundException.checkNullThrowNotFound(teacher, "Teacher", idTeacher);
        NotFoundException.checkNullThrowNotFound(subject, "Subject", idSubjectRun);
        teacherService.giveAssessmentToStudent(teacher, student, subject);
    }
}
