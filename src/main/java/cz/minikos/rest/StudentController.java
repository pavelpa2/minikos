package cz.minikos.rest;

import cz.minikos.exception.NotFoundException;
import cz.minikos.model.Student;
import cz.minikos.model.SubjectRun;
import cz.minikos.model.SubjectRunEnrollment;
import cz.minikos.security.SecurityUtils;
import cz.minikos.service.StudentService;
import cz.minikos.service.SubjectRunEnrollmentService;
import cz.minikos.service.TeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/student")
public class StudentController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final StudentService studentService;
    private final TeacherService teacherService;
    private final SubjectRunEnrollmentService subjectRunEnrollmentService;


    @Autowired
    public StudentController(StudentService studentService, TeacherService teacherService, SubjectRunEnrollmentService subjectRunEnrollmentService){
       this.studentService=studentService;
       this.teacherService=teacherService;
        this.subjectRunEnrollmentService = subjectRunEnrollmentService;
    }

    @GetMapping(value = "/SubjectRun")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public List<SubjectRun> getEnrolledSubjectRuns(){
        Student student = (Student) SecurityUtils.getCurrentUser();
        return studentService.getEnrolledSubjectRuns(student.getId());
    }

    @GetMapping(value = "/SubjectRunEnrollment")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public List<SubjectRunEnrollment> getSubjectRunEnrollments(){
        Student student = (Student) SecurityUtils.getCurrentUser();
        return studentService.getSubjectRunEnrollments(student.getId());
    }
}
