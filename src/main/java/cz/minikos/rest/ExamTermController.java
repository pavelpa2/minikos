package cz.minikos.rest;

import cz.minikos.exception.NotFoundException;
import cz.minikos.model.*;
import cz.minikos.security.SecurityUtils;
import cz.minikos.service.ExamEnrollmentService;
import cz.minikos.service.ExamTermService;
import cz.minikos.service.StudentService;
import cz.minikos.service.SubjectRunService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/rest/ExamTerm")
public class ExamTermController {
    private final ExamTermService examTermService;
    private final StudentService studentService;
    private final SubjectRunService subjectRunService;
    private final ExamEnrollmentService examEnrollmentService;

    @Autowired
    public ExamTermController(ExamTermService examTermService, StudentService studentService, SubjectRunService subjectRunService, ExamEnrollmentService examEnrollmentService) {
        this.examTermService = examTermService;
        this.studentService = studentService;
        this.subjectRunService = subjectRunService;
        this.examEnrollmentService = examEnrollmentService;
    }

//    @GetMapping(value = "/SubjectRun/{idSubjectRun}")
//    public List<ExamTerm> getExamTermOfSubjectRun(@PathVariable Integer idSubjectRun){
//        SubjectRun subjectRun = subjectRunService.find(idSubjectRun);
////        return examTermService
//    }

    @PostMapping(value = "/enroll")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ExamEnrollment enrollExam(@RequestBody ExamTerm examTermParam){
        ExamTerm examTerm = examTermService.find(examTermParam.getId());
        NotFoundException.checkNullThrowNotFound(examTerm, "ExamTerm", examTermParam.getId());
        Student student = (Student) SecurityUtils.getCurrentUser();
        return studentService.enrollExamTerm(student.getId(), examTerm);
    }

    @PostMapping(value = "/cancelEnrollment")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public void cancelExamEnrollment(@RequestBody ExamTerm examTermParam){
        ExamTerm examTerm = examTermService.find(examTermParam.getId());
        NotFoundException.checkNullThrowNotFound(examTerm, "ExamTerm", examTermParam.getId());
        Student student = (Student) SecurityUtils.getCurrentUser();
        studentService.cancelExamEnrollment(student.getId(), examTerm);
    }

    @PostMapping(value = "/enrollExtra")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public ExamEnrollment enrollExtraExam(@RequestBody ExamTerm examTermParam){
        ExamTerm examTerm = examTermService.find(examTermParam.getId());
        NotFoundException.checkNullThrowNotFound(examTerm, "ExamTerm", examTermParam.getId());
        Student student = (Student) SecurityUtils.getCurrentUser();
        return studentService.enrollExtraExamTerm(student.getId(), examTerm);
    }

    @PostMapping(value = "/cancelExtraEnrollment")
    @PreAuthorize("hasRole('ROLE_STUDENT')")
    public void cancelExtraExamEnrollment(@RequestBody ExamTerm examTermParam){
        ExamTerm examTerm = examTermService.find(examTermParam.getId());
        NotFoundException.checkNullThrowNotFound(examTerm, "ExamTerm", examTermParam.getId());
        Student student = (Student) SecurityUtils.getCurrentUser();
        studentService.cancelExamEnrollment(student.getId(), examTerm);
    }
}
