package cz.minikos.rest;

import cz.minikos.dummyDataGenerator.DummyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/generator")
public class GeneratorController {
    @Autowired
    DummyGenerator dummyGenerator;

    @GetMapping(value = "/generate")
    public void generate(){
//        dummyGenerator.generate();
    }
}
