package cz.minikos.util;

import cz.minikos.model.Semester;

public class Constants {

    public static final String CURRENT_SEMESTER_NAME = "b211";

    private Constants() {
        throw new AssertionError();
    }
}
