package cz.minikos.util;

import cz.minikos.model.ExamEnrollment;
import cz.minikos.model.Grade;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ExamEnrollmentGradeGivenAtTimeComparator implements Comparator<ExamEnrollment> {
    /**
     * Sorts by time of attribute getGradeGivenAt.
     * The most recent gradeGivenAt will end up at the beginning of the array/stream
     */
    @Override
    public int compare(ExamEnrollment o1, ExamEnrollment o2) {
        LocalDateTime l1 = o1.getGrade().getGradeGivenAt();
        LocalDateTime l2 = o2.getGrade().getGradeGivenAt();
        return l2.compareTo(l1);
    }

    public static Comparator<ExamEnrollment> getInstance() {
        return new ExamEnrollmentGradeGivenAtTimeComparator();
    }

    public ExamEnrollmentGradeGivenAtTimeComparator() {
    }
}
