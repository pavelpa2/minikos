package cz.minikos.model;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Subject.findByNameAndCode", query = "SELECT s from Subject s WHERE :name = s.name AND :code = s.code")
})
public class Subject extends AbstractEntity {
    @Column(nullable = false)
    private String code;
    @Column(nullable = false)
    private String name;

    public Subject() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
