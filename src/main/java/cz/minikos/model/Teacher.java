package cz.minikos.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("TEACHER")
public class Teacher extends User {
    public Teacher() {
        this.setRole(Role.TEACHER);
    }
}
