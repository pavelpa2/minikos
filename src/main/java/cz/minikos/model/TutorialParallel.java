package cz.minikos.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalTime;
import java.util.Date;

@Entity
public class TutorialParallel extends AbstractEntity {

    private LocalTime time;
    private String day;

    @ManyToOne
    private Teacher tutor;


    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Teacher getTutor() {
        return tutor;
    }

    public void setTutor(Teacher tutor) {
        this.tutor = tutor;
    }
}
