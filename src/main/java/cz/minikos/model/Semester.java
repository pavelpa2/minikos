package cz.minikos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Semester.findByName", query = "SELECT s from Semester s WHERE :name = s.name"),
})
public class Semester extends AbstractEntity {
    @Column(nullable = false, unique = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Semester semester = (Semester) o;
        return name.equals(semester.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
