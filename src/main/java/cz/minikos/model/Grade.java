package cz.minikos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Grade extends AbstractEntity {
    public enum GradeEnum {A,B,C,D,E,F,N}
    @Column(columnDefinition = "boolean default false")
    private boolean passed;
    private GradeEnum grade;
    private LocalDateTime gradeGivenAt;
    @Column(columnDefinition = "boolean default false")
    private boolean isGradeGiven;

    @ManyToOne
    private Teacher gradeGiver;

    public GradeEnum getGrade() {
        return grade;
    }

    public Grade setGrade(GradeEnum grade) {
        this.grade = grade;
        return this;
    }

    public Teacher getGradeGiver() {
        return gradeGiver;
    }

    public Grade setGradeGiver(Teacher gradeGiver) {
        this.gradeGiver = gradeGiver;
        return this;
    }

    public Grade setGradeGivenAt(LocalDateTime assessmentGivenAt) {
        this.gradeGivenAt = assessmentGivenAt;
        return this;
    }

    public LocalDateTime getGradeGivenAt() {
        return gradeGivenAt;
    }

    public boolean isGradeGiven() {
        return isGradeGiven;
    }

    public void setGradeGiven(boolean gradeGiven) {
        isGradeGiven = gradeGiven;
    }

    public boolean didPass() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }
}
