package cz.minikos.model;

import javax.persistence.*;

@Entity
public class ExamEnrollment extends AbstractEntity{
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Grade grade;
    @ManyToOne
    private ExamTerm examTerm;

    public ExamEnrollment(){

    }

    public ExamEnrollment(ExamTerm examTerm){
        this.examTerm = examTerm;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public ExamTerm getExamTerm() {
        return examTerm;
    }

    public void setExamTerm(ExamTerm examTerm) {
        this.examTerm = examTerm;
    }

}
