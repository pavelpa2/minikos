package cz.minikos.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "SubjectRun.findBySemester", query = "SELECT s from SubjectRun s WHERE :semesterName = s.semester.name"),
        @NamedQuery(name = "SubjectRun.findBySubjectAndSemester", query = "SELECT s from SubjectRun s WHERE :semester = s.semester AND :subjectName = s.subject.name AND :subjectCode = s.subject.code")
})
public class SubjectRun extends AbstractEntity {
    @Column(nullable = false)
    private int credits;

    @ManyToOne
    private Subject subject;

    @ManyToMany
    @JoinTable(name = "subjectRun_tutors")
    private List<Teacher> tutors;

    @ManyToMany
    @JoinTable(name = "subjectRun_lecturers")
    private List<Teacher> lecturers;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(nullable = false)
    private Semester semester;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn
    private TutorialParallel tutorialParallel;

    public SubjectRun() {
    }

    public SubjectRun(int credits, List<Teacher> tutors, List<Teacher> lecturers, Semester semester, TutorialParallel tutorialParallel) {
        this.credits = credits;
        this.tutors = tutors;
        this.lecturers = lecturers;
        this.semester = semester;
        this.tutorialParallel = tutorialParallel;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public List<Teacher> getTutors() {
        return tutors;
    }

    public void setTutors(List<Teacher> tutors) {
        this.tutors = tutors;
    }

    public List<Teacher> getLecturers() {
        return lecturers;
    }

    public void setLecturers(List<Teacher> lecturers) {
        this.lecturers = lecturers;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public TutorialParallel getTutorialParallel() {
        return tutorialParallel;
    }

    public void setTutorialParallel(TutorialParallel tutorialParallel) {
        this.tutorialParallel = tutorialParallel;
    }

    public void addTutor(Teacher t) {
        this.addTeacher(t, this.getTutors());
    }

    public void addLecturer(Teacher t) {
        this.addTeacher(t, this.getLecturers());
    }

    private void addTeacher (Teacher t, List<Teacher> listToAddTo) {
        if (listToAddTo == null) {
            listToAddTo = new ArrayList<>();
        } else if (listToAddTo.contains(t)) {
            return;
        }
        listToAddTo.add(t);
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
