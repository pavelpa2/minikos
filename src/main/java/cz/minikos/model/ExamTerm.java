package cz.minikos.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class ExamTerm extends AbstractEntity {
    private LocalDateTime date;
    @ManyToOne
    private Teacher lecturer;
    @ManyToOne
    private SubjectRun subjectRun;

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Teacher getLecturer() {
        return lecturer;
    }

    public void setLecturer(Teacher lecturer) {
        this.lecturer = lecturer;
    }

    public SubjectRun getSubjectRun() {
        return subjectRun;
    }

    public void setSubjectRun(SubjectRun subjectRun) {
        this.subjectRun = subjectRun;
    }
}
