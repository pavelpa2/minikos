package cz.minikos.model;

import javax.persistence.*;
@Entity
// We can't name the table User, as it is a reserved table name in some dbs, including Postgres
@Table(name = "EAR_USER")
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
})
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "USER_TYPE")
public class User extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String username;

    @Basic(optional = false)
    @Column(nullable = false, updatable = false)
    private String password;

    @Column
    private String name;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User() {
        this.role = Role.GUEST;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isStudent(){
        return this.role == Role.STUDENT;
    }
    public boolean isTeacher(){
        return this.role == Role.TEACHER;
    }
    public boolean isAdmin(){
        return this.role == Role.ADMIN;
    }

    public void erasePassword(){
        this.password = null;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", role=" + role +
                '}';
    }
}
