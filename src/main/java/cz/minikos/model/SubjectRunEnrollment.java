package cz.minikos.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;


@Entity
public class SubjectRunEnrollment extends AbstractEntity {
    @Column(columnDefinition = "boolean default false")
    private boolean assessmentGiven;
    private LocalDateTime assessmentGivenAt;
    @ManyToOne
    private Teacher assessmentGiver;
    @ManyToOne
    private TutorialParallel tutorialParallel;
    @JoinColumn(nullable = false)
    @ManyToOne
    private SubjectRun subjectRun;


    public boolean isAssessmentGiven() {
        return assessmentGiven;
    }

//    public void setAssessmentGiven(boolean assessmentGiven) {
//        this.assessmentGiven = assessmentGiven;
//    }

    public LocalDateTime getAssessmentGivenAt() {
        return assessmentGivenAt;
    }

//    public void setAssessmentGivenAt(Date assessmentGivenAt) {
//        this.assessmentGivenAt = assessmentGivenAt;
//    }

    public Teacher getAssessmentGiver() {
        return assessmentGiver;
    }

//    public void setAssessmentGiver(Teacher assessmentGiver) {
//        this.assessmentGiver = assessmentGiver;
//    }

    public TutorialParallel getTutorialParallel() {
        return tutorialParallel;
    }

//    public void setTutorialParallel(TutorialParallel tutorialParallel) {
//        this.tutorialParallel = tutorialParallel;
//    }

    public SubjectRun getSubjectRun() {
        return subjectRun;
    }

//    public void setSubjectRun(SubjectRun subjectRun) {
//        this.subjectRun = subjectRun;
//    }

    public SubjectRunEnrollment setAssessmentGiven(boolean assessmentGiven) {
        this.assessmentGiven = assessmentGiven;
        return this;
    }
    public SubjectRunEnrollment setAssessmentGivenAt(LocalDateTime assessmentGivenAt) {
        this.assessmentGivenAt = assessmentGivenAt;
        return this;
    }
    public SubjectRunEnrollment setAssessmentGiver(Teacher assessmentGiver) {
        this.assessmentGiver = assessmentGiver;
        return this;
    }
    public SubjectRunEnrollment setTutorialParallel(TutorialParallel tutorialParallel) {
        this.tutorialParallel = tutorialParallel;
        return this;
    }
    public SubjectRunEnrollment setSubjectRun(SubjectRun subjectRun) {
        this.subjectRun = subjectRun;
        return this;
    }





}
