package cz.minikos.model;

import cz.minikos.exception.ExamTermDueException;
import cz.minikos.exception.EnrollmentException;
import cz.minikos.exception.MiniKOSException;
import cz.minikos.util.ExamEnrollmentGradeGivenAtTimeComparator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@DiscriminatorValue("STUDENT")
public class Student extends User {

    public Student() {
        this.setRole(Role.STUDENT);
    }

    @Column(columnDefinition = "integer default 6")
    private int extraExamEnrollmentsAvailable;

    @ManyToMany
    @OrderBy("assessmentGiven ASC")
    private List<SubjectRunEnrollment> subjectRunEnrollments;

    @ManyToMany
    private List<ExamEnrollment> examEnrollments;

    public List<SubjectRunEnrollment> getSubjectRunEnrollments() {
        return subjectRunEnrollments;
    }

    public void setSubjectRunEnrollments(List<SubjectRunEnrollment> subjectRunEnrollments) {
        this.subjectRunEnrollments = subjectRunEnrollments;
    }

    @Override
    public void setRole(Role role) {
        if (role == Role.ADMIN) {
            return;
        }
        super.setRole(role);
    }

    public List<ExamEnrollment> getExamEnrollments() {
        return examEnrollments;
    }

    public void setExamEnrollments(List<ExamEnrollment> examEnrollments) {
        this.examEnrollments = examEnrollments;
    }

    public int getExtraExamEnrollmentsAvailable() {
        return extraExamEnrollmentsAvailable;
    }

    public void setExtraExamEnrollmentsAvailable(int extraExamEnrollmentsAvailable) {
        if (extraExamEnrollmentsAvailable <= 6){
        this.extraExamEnrollmentsAvailable = extraExamEnrollmentsAvailable;
        } else {
            throw new MiniKOSException("Student can't have more than 6 extra exam enrollments");
        }
    }


    public int getNumberOfGradedExamEnrollmentsFromSubject(Subject subject, String semester){
        return (int) getGradedExamEnrollmentsStream(subject, semester).count();
    }

    public ExamEnrollment cancelExamEnrollment(Subject subject, String semester) {
        ExamEnrollment examEnrollment = getActiveExamEnrollmentFromSubjectAtSemester(subject, semester);
        if (examEnrollment == null) {
            throw new EnrollmentException("Student is not enrolled to exam.");
        }
        boolean examTermIsDue = isExamTermDue(examEnrollment.getExamTerm());
        if (examTermIsDue) {
            throw new ExamTermDueException("Student cannot cancel enrollment because exam term is due");
        }
        this.getExamEnrollments().remove(examEnrollment);
        return examEnrollment;
    }

    public ExamEnrollment cancelExtraExamEnrollment(Subject subject, String semester) {
        ExamEnrollment examEnrollment = getActiveExamEnrollmentFromSubjectAtSemester(subject, semester);
        if (examEnrollment == null) {
            throw new EnrollmentException("Student is not enrolled to exam.");
        }
        boolean examTermIsDue = isExamTermDue(examEnrollment.getExamTerm());
        if (examTermIsDue) {
            throw new ExamTermDueException("Student cannot cancel enrollment because exam term is due");
        }
        this.setExtraExamEnrollmentsAvailable(this.getExtraExamEnrollmentsAvailable() + 1);
        this.getExamEnrollments().remove(examEnrollment);
        return examEnrollment;
    }

    private boolean isExamTermDue(ExamTerm examTerm) {
        LocalDateTime examTermDate = examTerm.getDate();
        LocalDateTime now = LocalDateTime.now();
        return examTermDate.isBefore(now);
    }

    /**
     * Is student enrolled in exam term
     * Student is enrolled in exam term, if ExamEnrollment without grade exists.
     */
    public boolean doesStudentHaveActiveExamEnrollment(Subject subject, String semester) {
        ExamEnrollment examEnrollment = getActiveExamEnrollmentFromSubjectAtSemester(subject, semester);
        return examEnrollment != null;
    }

    /**
     * Get last graded ExamEnrollment. If last graded Exam Enrollment has a grade better or equal to E, student has passed subject
     */
    public boolean didStudentPassSubject(Subject subject, String semester) {
        ExamEnrollment lastGraded = getLastGradedExamEnrollmentFromSubjectAtSemester(subject, semester);
        if (lastGraded == null) {
            return false;
        }
        return lastGraded.getGrade().didPass();
    }

    /**
     * Check list of exam enrollments and return one that has been graded as the last.
     */
    public ExamEnrollment getLastGradedExamEnrollmentFromSubjectAtSemester(Subject subject, String semester) {
        return getGradedExamEnrollmentsStream(subject, semester)
                .sorted(
                        ExamEnrollmentGradeGivenAtTimeComparator
                                .getInstance()
                )
                .collect(
                        Collectors
                                .toList()
                )
                .get(0);
    }

    private Stream<ExamEnrollment> getGradedExamEnrollmentsStream(Subject subject, String semester) {
        return getExamEnrollmentsAtSemesterStream(subject, semester).filter(e->e.getGrade().isGradeGiven());
    }

    /**
     * Should be only one. ExamEnrollment where Grade wasn't given yet.
     */
    public ExamEnrollment getActiveExamEnrollmentFromSubjectAtSemester(Subject subject, String semester) {
        List<ExamEnrollment> list = getExamEnrollmentsAtSemesterStream(subject, semester).filter(e-> !e.getGrade().isGradeGiven()).collect(Collectors.toList());
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public List<ExamEnrollment> getExamEnrollmentsAtSemester(Subject subject, String semesterName) {
        return getExamEnrollmentsAtSemesterStream(subject, semesterName)
                .collect(
                        Collectors.toList()
                );
    }

    public SubjectRunEnrollment getSubjectRunEnrollmentOfSubjectRun(SubjectRun subjectRun, Semester semester){
        Optional<SubjectRunEnrollment> subjectRunEnrollment = getEnrolledSubjectRunsStream(semester).filter(
                e->
                        e.getSubjectRun().getSubject().getName() == subjectRun.getSubject().getName()
                &&
                                e.getSubjectRun().getSubject().getCode() == subjectRun.getSubject().getCode()
        ).findFirst();
        return subjectRunEnrollment.orElse(null);
    }

    private Stream<ExamEnrollment> getExamEnrollmentsAtSemesterStream(Subject subject, String semesterName) {
        String subjectName = subject.getName();
        String subjectCode = subject.getCode();
        if (examEnrollments == null) {
            return null;
        }
        return getExamEnrollments()
                .stream()
                .filter(
                        e-> Objects.equals(e
                                .getExamTerm()
                                .getSubjectRun()
                                .getSemester().getName(), semesterName)
                                && e
                                .getExamTerm()
                                .getSubjectRun()
                                .getSubject()
                                    .getCode()
                                .equals(subjectCode)
                                && e
                                .getExamTerm()
                                .getSubjectRun()
                                .getSubject()
                                .getName()
                                .equals(subjectName)
                );
    }

    public boolean canStudentEnrollToAnExtraExamTerm() {
        return getExtraExamEnrollmentsAvailable() > 0;
    }

    /**
     * Get number of enrolled credits at specified semester
     */
    public int getNumberOfEnrolledCredits(Semester semester) {
        return getEnrolledSubjectRunsStream(semester)
                .mapToInt(
                e -> e.getSubjectRun()
                        .getCredits()
                )
                .sum();
    }

    /**
     * Get enrolled subjects at specific semester
     */
    public List<SubjectRunEnrollment> getSubjectRunEnrollments(Semester semester) {
        return getEnrolledSubjectRunsStream(semester).collect(Collectors.toList());
    }
    /**
     * Get enrolled subjects at specific semester
     */
    public List<SubjectRun> getEnrolledSubjectRuns(Semester semester) {
        return getEnrolledSubjectRunsStream(semester).map(examEnrollments -> examEnrollments.getSubjectRun()).collect(Collectors.toList());
    }

    private Stream<SubjectRunEnrollment> getEnrolledSubjectRunsStream(Semester semester) {
        return getSubjectRunEnrollments()
                .stream()
                .filter(
                        e -> e.getSubjectRun()
                                .getSemester()
                                .equals(semester)
                );
    }

    /**
     * Get number of times student enrolled in a Subject since beginning of times.
     */
    public int getHowManyTimesStudentWasEnrolledInASubject(Subject subject) {
        String name = subject.getName();
        String code = subject.getCode();
        return getHowManyTimesStudentWasEnrolledInASubject(name, code);
    }

    /**
     * Get number of times student enrolled in a Subject since beginning of times.
     */
    public int getHowManyTimesStudentWasEnrolledInASubject(String subjectName, String subjectCode) {
        return (int) getSubjectRunEnrollments()
                .stream()
                .filter(
                        e -> e.getSubjectRun()
                                .getSubject()
                                .getName()
                                .equals(subjectName)
                                &&
                                e.getSubjectRun()
                                        .getSubject()
                                        .getCode()
                                        .equals(subjectCode)
                )
                .count();
    }

    public int getHowManyTimesStudentWasEnrolledInAExamAtSemester(Subject subjectRun, String semesterName) {
        String code = subjectRun.getCode();
        String name = subjectRun.getName();
        return (int) getExamEnrollments()
                .stream()
                .filter(
                        e -> e.getExamTerm()
                        .getSubjectRun().getSubject()
                        .getCode()
                        .equals(code)
                        &&
                        e.getExamTerm()
                                .getSubjectRun()
                                .getSubject()
                                .getName()
                                .equals(name)
                                &&
                        e.getExamTerm()
                        .getSubjectRun()
                        .getSemester()
                        .getName()
                        .equals(semesterName)
                )
                .count();
    }

    public boolean didStudentReceiveAssessmentFromSubjectRun(Subject subject, String semester) {
        return getSubjectRunEnrollmentStream(subject, semester).anyMatch(SubjectRunEnrollment::isAssessmentGiven);
    }

    private Stream<SubjectRunEnrollment> getSubjectRunEnrollmentStream (Subject subject, String semester) {
        String name = subject.getName();
        String code = subject.getCode();
        return getSubjectRunEnrollments()
                .stream()
                .filter(
                        e -> e.getSubjectRun()
                                .getSubject()
                                .getName()
                                .equals(name)
                                &&
                                e.getSubjectRun()
                                        .getSubject()
                                        .getCode()
                                        .equals(code)
                                &&
                                Objects.equals(e.getSubjectRun()
                                        .getSemester().getName(), semester)
                );
    }

    /**
     * Is/was student enrolled to a subject at specified semester
     */
    public boolean isStudentEnrolledToSubjectAtSemester(Subject subject, String semesterName) {
        String name = subject.getName();
        String code = subject.getCode();
        return getSubjectRunEnrollments()
                .stream()
                .anyMatch(
                        e -> e.getSubjectRun()
                                .getSubject()
                                .getName()
                                .equals(name)
                                &&
                                e.getSubjectRun()
                                        .getSubject()
                                        .getCode()
                                        .equals(code)
                                &&
                                Objects.equals(e.getSubjectRun().getSemester().getName(), semesterName)
                );
    }
}
