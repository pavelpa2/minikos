package cz.minikos.exception;

public class UserAlreadyRegistered extends MiniKOSException{
    public UserAlreadyRegistered () {
    }
    public UserAlreadyRegistered (String message) {
        super(message);
    }
}
