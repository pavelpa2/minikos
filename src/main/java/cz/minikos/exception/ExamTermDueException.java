package cz.minikos.exception;

public class ExamTermDueException extends MiniKOSException{
    public ExamTermDueException() {
    }

    public ExamTermDueException(String message) {
        super(message);
    }
}
