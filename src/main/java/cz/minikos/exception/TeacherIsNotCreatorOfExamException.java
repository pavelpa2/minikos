package cz.minikos.exception;

public class TeacherIsNotCreatorOfExamException extends  MiniKOSException{
    public TeacherIsNotCreatorOfExamException() {
    }

    public TeacherIsNotCreatorOfExamException(String message) {
        super(message);
    }
}
