package cz.minikos.exception;
public class StudentIsNotEnrolled extends MiniKOSException{
    public StudentIsNotEnrolled () { }

    public StudentIsNotEnrolled(String message) {
        super(message);
    }
}
