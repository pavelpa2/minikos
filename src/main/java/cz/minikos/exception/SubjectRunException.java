package cz.minikos.exception;

public class SubjectRunException extends MiniKOSException{
    public SubjectRunException() {
    }

    public SubjectRunException(String message) {
        super(message);
    }
}
