package cz.minikos.exception;

public class StudentCannotEnrollToSubjectRunAgain extends MiniKOSException{
    public StudentCannotEnrollToSubjectRunAgain  () {
    }

    public StudentCannotEnrollToSubjectRunAgain (String message) {
        super(message);
    }
}
