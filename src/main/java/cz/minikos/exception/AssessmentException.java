package cz.minikos.exception;

public class AssessmentException extends MiniKOSException {
    public AssessmentException() {
    }

    public AssessmentException(String message) {
        super(message);
    }
}
