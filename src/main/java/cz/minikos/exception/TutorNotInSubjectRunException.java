package cz.minikos.exception;

public class TutorNotInSubjectRunException extends  MiniKOSException{
    public TutorNotInSubjectRunException() {
    }

    public TutorNotInSubjectRunException(String message) {
        super(message);
    }
}
