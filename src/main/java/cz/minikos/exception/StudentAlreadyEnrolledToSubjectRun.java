package cz.minikos.exception;

public class StudentAlreadyEnrolledToSubjectRun extends MiniKOSException {

    public StudentAlreadyEnrolledToSubjectRun () {
    }

    public StudentAlreadyEnrolledToSubjectRun (String message) {
        super(message);
    }
}
