package cz.minikos.exception;

public class StudentCannotEnrollToExamTerm extends MiniKOSException{
    public StudentCannotEnrollToExamTerm() {
    }

    public StudentCannotEnrollToExamTerm(String message) {
        super(message);
    }
}
