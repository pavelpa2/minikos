package cz.minikos.exception;

public class StudentCannotCancelExamEnrollment extends MiniKOSException{
    public StudentCannotCancelExamEnrollment () {
    }

    public StudentCannotCancelExamEnrollment(String message) {
        super(message);
    }
}
