package cz.minikos.exception;

public class EnrollmentException extends MiniKOSException{
    public EnrollmentException() {
    }

    public EnrollmentException(String message) {
        super(message);
    }
}
