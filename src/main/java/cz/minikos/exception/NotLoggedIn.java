package cz.minikos.exception;

public class NotLoggedIn extends MiniKOSException{

    public NotLoggedIn() {
    }

    public NotLoggedIn(String message) {
        super(message);
    }
}
