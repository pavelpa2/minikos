package cz.minikos.exception;

public class TeacherCantGiveAssessment extends MiniKOSException{
    public TeacherCantGiveAssessment () {
    }

    public TeacherCantGiveAssessment(String message) {
        super(message);
    }
}
