package cz.minikos.exception;

public class MiniKOSException extends RuntimeException{
    public MiniKOSException() {
    }

    public MiniKOSException(String message) {
        super(message);
    }

    public MiniKOSException(String message, Throwable cause) {
        super(message, cause);
    }

    public MiniKOSException(Throwable cause) {
        super(cause);
    }
}
