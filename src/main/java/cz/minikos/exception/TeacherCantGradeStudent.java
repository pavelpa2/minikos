package cz.minikos.exception;

public class TeacherCantGradeStudent extends MiniKOSException{
    public TeacherCantGradeStudent () {
    }

    public TeacherCantGradeStudent(String message) {
        super(message);
    }
}
