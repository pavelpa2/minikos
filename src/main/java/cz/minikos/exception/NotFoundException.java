package cz.minikos.exception;

import cz.minikos.model.AbstractEntity;

/**
 * Indicates that a resource was not found.
 */
public class NotFoundException extends MiniKOSException {

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public static NotFoundException create(String resourceName, Object identifier) {
        return new NotFoundException(resourceName + " identified by " + identifier + " not found.");
    }

    public static void checkNullThrowNotFound(AbstractEntity entity, String className, Integer id){
        if (entity == null){
            throw NotFoundException.create(className, id);
        }
    }
    public static void checkNullThrowNotFound(AbstractEntity entity, String className, String id){
        if (entity == null){
            throw NotFoundException.create(className, id);
        }
    }
}
