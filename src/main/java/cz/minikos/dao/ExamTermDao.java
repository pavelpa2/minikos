package cz.minikos.dao;

import cz.minikos.model.ExamTerm;
import org.springframework.stereotype.Repository;

@Repository
public class ExamTermDao extends BaseDao<ExamTerm> {

    public ExamTermDao() {
        super(ExamTerm.class);
    }
}
