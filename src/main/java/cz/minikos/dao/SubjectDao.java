package cz.minikos.dao;

import cz.minikos.model.Subject;
import cz.minikos.model.SubjectRun;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class SubjectDao extends BaseDao<Subject> {

    public SubjectDao(){
        super(Subject.class);
    }

    public Subject find(String name, String code) {
        Subject s;
        try {
            s = em.createNamedQuery("Subject.findByNameAndCode", Subject.class).setParameter("name", name).setParameter("code", code)
                    .getSingleResult();
            return s;
        } catch (NoResultException e) {
            return null;
        }
    }
}