package cz.minikos.dao;

import cz.minikos.model.Semester;
import cz.minikos.model.Subject;
import cz.minikos.model.SubjectRun;
import cz.minikos.service.SubjectService;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class SubjectRunDao extends BaseDao<SubjectRun> {

    public SubjectRunDao(){
        super(SubjectRun.class);
    }

    public SubjectRun find(String name, String code, Semester semester) {
        Objects.requireNonNull(semester);
        Objects.requireNonNull(name);
        Objects.requireNonNull(code);
        SubjectRun subjectRun;
        try {
            subjectRun = em.createNamedQuery("SubjectRun.findBySubjectAndSemester", SubjectRun.class).setParameter("semester", semester).setParameter("subjectName", name).setParameter("subjectCode", code)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return subjectRun;
    }

    public SubjectRun find(Subject subject, Semester semester) {
        Objects.requireNonNull(semester);
        Objects.requireNonNull(subject);
        return em.createNamedQuery("SubjectRun.findBySubjectAndSemester", SubjectRun.class).setParameter("semester", semester).setParameter("subjectName", subject.getName()).setParameter("subjectCode", subject.getCode())
                .getSingleResult();
    }

    public List<SubjectRun> findAll(Semester semester) {
        Objects.requireNonNull(semester);
        return em.createNamedQuery("SubjectRun.findBySemester", SubjectRun.class).setParameter("semesterName", semester.getName())
                .getResultList();
    }
}