package cz.minikos.dao;

import cz.minikos.model.TutorialParallel;
import org.springframework.stereotype.Repository;

@Repository
public class TutorialParallelDao extends BaseDao<TutorialParallel> {

    public TutorialParallelDao(){
        super(TutorialParallel.class);
    }
}