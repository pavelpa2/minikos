package cz.minikos.dao;

import cz.minikos.model.SubjectRunEnrollment;
import org.springframework.stereotype.Repository;

@Repository
public class SubjectRunEnrollmentDao extends BaseDao<SubjectRunEnrollment> {

    public SubjectRunEnrollmentDao(){
        super(SubjectRunEnrollment.class);
    }
}