package cz.minikos.dao;

import cz.minikos.model.Grade;
import org.springframework.stereotype.Repository;

@Repository
public class GradeDao extends BaseDao<Grade> {

    public GradeDao() {
        super(Grade.class);
    }
}
