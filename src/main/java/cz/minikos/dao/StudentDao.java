package cz.minikos.dao;

import cz.minikos.model.Student;
import org.springframework.stereotype.Repository;

@Repository
public class StudentDao extends BaseDao<Student> {

    public StudentDao() {
        super(Student.class);
    }
}