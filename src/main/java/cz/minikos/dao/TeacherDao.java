package cz.minikos.dao;

import cz.minikos.model.Teacher;
import org.springframework.stereotype.Repository;

@Repository
public class TeacherDao extends BaseDao<Teacher> {

    public TeacherDao(){
        super(Teacher.class);
    }
}