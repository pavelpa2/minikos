package cz.minikos.dao;

import cz.minikos.model.ExamEnrollment;
import org.springframework.stereotype.Repository;

@Repository
public class ExamEnrollmentDao extends BaseDao<ExamEnrollment> {

    public ExamEnrollmentDao() {
        super(ExamEnrollment.class);
    }
}
