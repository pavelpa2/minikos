package cz.minikos.dao;

import cz.minikos.model.Semester;
import cz.minikos.model.SubjectRun;
import org.springframework.stereotype.Repository;

@Repository
public class SemesterDao extends BaseDao<Semester> {

    public SemesterDao() {
        super(Semester.class);
    }

    public Semester find(String name){
        Semester semester;
        try {
            semester = em.createNamedQuery("Semester.findByName", Semester.class).setParameter("name", name)
                    .getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return semester;
    }
}
