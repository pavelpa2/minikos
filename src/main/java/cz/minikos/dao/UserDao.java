package cz.minikos.dao;

import cz.minikos.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.util.List;

@Repository
public class UserDao extends BaseDao<User> {

    public UserDao() {
        super(User.class);
    }

    public User findByUsername(String username) {
        try {
            return em.createNamedQuery("User.findByUsername", User.class).setParameter("username", username)
                     .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    @Override
    public List<User> findAll() {
        try {
            return em.createQuery("SELECT e FROM " + "User e", type).getResultList();
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
