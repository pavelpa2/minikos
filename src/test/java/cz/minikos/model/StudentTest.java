package cz.minikos.model;

import cz.minikos.environment.Generator;
import cz.minikos.util.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class StudentTest {
    @PersistenceContext
    private EntityManager em;

    ExamEnrollment moreRecentExamEnrollment;
    ExamEnrollment olderExamEnrollment;

    Student student;

    SubjectRun subjectRun1;

    String semester1;

    @BeforeEach
    public void setUp() {
        student = Generator.generateStudent();
        subjectRun1 = Generator.generateSubjectRun();
        semester1 = Constants.CURRENT_SEMESTER_NAME;
        Semester semester = new Semester();
        semester.setName(semester1);
        subjectRun1.setSemester(semester);

        em.persist(subjectRun1);

        Grade olderGrade = new Grade();
        olderGrade.setGradeGiven(true);
        olderGrade.setPassed(true);
        olderGrade.setGrade(Grade.GradeEnum.A);
        olderGrade.setGradeGivenAt(LocalDateTime.of(2020, 1, 1, 3, 4, 5));

        Grade moreRecentGrade = new Grade();
        moreRecentGrade.setGradeGiven(true);
        moreRecentGrade.setPassed(true);
        moreRecentGrade.setGrade(Grade.GradeEnum.B);
        moreRecentGrade.setGradeGivenAt(LocalDateTime.of(2020, 1, 2, 3, 4, 5));

        ExamTerm olderExamTerm = new ExamTerm();
        olderExamTerm.setSubjectRun(subjectRun1);

        ExamTerm moreRecentExamTerm = new ExamTerm();
        moreRecentExamTerm.setSubjectRun(subjectRun1);

        em.persist(olderExamTerm);
        em.persist(moreRecentExamTerm);

        olderExamEnrollment = new ExamEnrollment();
        olderExamEnrollment.setGrade(olderGrade);
        olderExamEnrollment.setExamTerm(olderExamTerm);



        moreRecentExamEnrollment = new ExamEnrollment();
        moreRecentExamEnrollment.setGrade(moreRecentGrade);
        moreRecentExamEnrollment.setExamTerm(moreRecentExamTerm);

        em.persist(olderExamEnrollment);
        em.persist(moreRecentExamEnrollment);

        student.setExamEnrollments(new ArrayList<>());
        student.getExamEnrollments().add(moreRecentExamEnrollment);
        student.getExamEnrollments().add(olderExamEnrollment);
        em.persist(student);
    }


    @Test
    public void test_getLastGradedExamEnrollment() {
        assertEquals(moreRecentExamEnrollment, student.getLastGradedExamEnrollmentFromSubjectAtSemester(subjectRun1.getSubject(), semester1));
    }

    @Test
    public void test_isStudentEnrolledInExamTerm_false() {
        assertFalse(student.doesStudentHaveActiveExamEnrollment(subjectRun1.getSubject(), semester1));
    }

    @Test
    public void test_isStudentEnrolledInExamTerm_true() {
        moreRecentExamEnrollment.getGrade().setGradeGiven(false);
        assertTrue(student.doesStudentHaveActiveExamEnrollment(subjectRun1.getSubject(), semester1));
    }

    @Test
    public void test_didStudentPassSubject_true() {
        assertTrue(student.didStudentPassSubject(subjectRun1.getSubject(), semester1));
    }

    @Test
    public void test_didStudentPassSubject_false() {
        moreRecentExamEnrollment.getGrade().setGrade(Grade.GradeEnum.F);
        assertTrue(student.didStudentPassSubject(subjectRun1.getSubject(), semester1));
    }

    @Test
    public void test_didStudentPassSubjectWhenHeFailedFirstTerm_true() {
        olderExamEnrollment.getGrade().setGrade(Grade.GradeEnum.F);
        assertTrue(student.didStudentPassSubject(subjectRun1.getSubject(), semester1));
    }
}
