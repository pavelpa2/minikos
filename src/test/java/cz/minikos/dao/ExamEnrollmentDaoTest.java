package cz.minikos.dao;

import cz.minikos.model.ExamEnrollment;
import cz.minikos.model.Grade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class ExamEnrollmentDaoTest {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    ExamEnrollmentDao dao;

    @Autowired
    GradeDao gradeDao;

    @Test
    public void gradeGetsCreatedOnPersist() {
        ExamEnrollment examEnrollment = new ExamEnrollment();
        examEnrollment.setGrade(new Grade());
        Grade g = examEnrollment.getGrade();
        dao.persist(examEnrollment);
        Grade resGrade = dao.find(examEnrollment.getId()).getGrade();
        assertEquals(
                g,
                resGrade
        );
        assertNotEquals(null, resGrade.getId());
    }

    @Test
    public void gradeGetsRemovedWhenExamEnrollmentGetsDeleted() {
        // set up
        ExamEnrollment examEnrollment = new ExamEnrollment();
        examEnrollment.setGrade(new Grade());

        Grade g = examEnrollment.getGrade();

        dao.persist(examEnrollment);
        Grade resGrade = dao.find(examEnrollment.getId()).getGrade();

        dao.remove(examEnrollment);

        Grade gradeInRepo = gradeDao.find(resGrade.getId());

        assertNull(gradeInRepo);
    }
}
