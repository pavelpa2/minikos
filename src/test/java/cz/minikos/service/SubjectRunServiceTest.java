package cz.minikos.service;

import cz.minikos.dao.SubjectRunDao;
import cz.minikos.environment.Generator;
import cz.minikos.exception.MiniKOSException;
import cz.minikos.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class SubjectRunServiceTest {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private SubjectRunService sut;
    @Autowired
    private SubjectRunDao dao;
    @Autowired
    private SubjectService subjectService;

    String name;
    String code;
    Semester semester;
    int credits;

    @BeforeEach
    private void setUp(){
        name = "Calculus1";
        code = "MA1";
        credits = 5;
        semester = Generator.generateSemester();
        em.persist(semester);
    }

    @Test
    public void test_createSubjectRunOfNonExistingSubject_throwsException() {
        assertThrows(MiniKOSException.class, ()->sut.createSubjectRun("nonExistingName", "nonExistingCode", semester, 5));
    }

    @Test
    public void test_createSubjectRun_returnsSubjectRun() {
        Subject s = subjectService.createSubject("uniqueName", "uniqueCode");
        SubjectRun subjectRun = sut.createSubjectRun(s, semester, 6 );
        assertEquals(s, subjectRun.getSubject());
    }


//    @Test
//    public void test_updateTutorialParallel_updatesTutorialParallelInSubjectRunEnrollment() {
//
//        Teacher teacher1 = Generator.generateTeacher();
//        Teacher teacher2 = Generator.generateTeacher();
//
//        em.persist(teacher1);
//        em.persist(teacher2);
//
//        TutorialParallel tutorialParallel = Generator.generateTutorialParallel(teacher1);
//        em.persist(tutorialParallel);
//
//        SubjectRun subjectRun = sut.createSubjectRun(name, code, semester, credits);
//        sut.addTutor(subjectRun,teacher1);
//        sut.addTutor(subjectRun, teacher2);
//        subjectRun.setTutorialParallel(tutorialParallel);
//
//        SubjectRunEnrollment subjectRunEnrollment = new SubjectRunEnrollment();
//        subjectRunEnrollment.setSubjectRun(subjectRun);
//        subjectRunEnrollment.setTutorialParallel(tutorialParallel);
//        em.persist(subjectRunEnrollment);
//
//
//        TutorialParallel updatedTutorialParallel = Generator.generateTutorialParallel(teacher2);
//        sut.updateTutorialParallel(subjectRun, updatedTutorialParallel);
//
//        assertEquals(subjectRunEnrollment.getTutorialParallel(), subjectRun.getTutorialParallel());
//    }
}
