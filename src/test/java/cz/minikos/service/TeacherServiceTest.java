package cz.minikos.service;

import cz.minikos.dao.SubjectRunDao;
import cz.minikos.dao.TeacherDao;
import cz.minikos.environment.Generator;
import cz.minikos.exception.AssessmentException;
import cz.minikos.exception.TeacherCantGiveAssessment;
import cz.minikos.model.Student;
import cz.minikos.model.SubjectRun;
import cz.minikos.model.SubjectRunEnrollment;
import cz.minikos.model.Teacher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class TeacherServiceTest {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    TeacherDao dao;
    @Autowired
    private TeacherService sut;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SubjectRunDao subjectRunDao;
    @Autowired
    private SubjectRunService subjectRunService;
    @Autowired
    private TutorialParallelService tutorialParallelService;

    private Teacher t1;
    private Teacher t2;
    private Student s1;
    private SubjectRun subjectRun;

    @BeforeEach
    public void setUp() {
        this.t1 = Generator.generateTeacher();
        this.t2 = Generator.generateTeacher();
        this.s1 = Generator.generateStudent();
        this.subjectRun = Generator.generateSubjectRun();

        em.persist(t1);
        em.persist(t2);
        em.persist(s1);
    }

    @Test
    public void test_teacherThatIsTutorOfSubjectParallelGivesAssessment_succeeds() {
        subjectRunService.addTutor(this.subjectRun, t1);
        subjectRun.setTutorialParallel(tutorialParallelService.createTutorialParallel(
            "MON",
                LocalTime.now(),
                t1
        ));
        SubjectRunEnrollment subjectRunEnrollment = studentService.enrollSubject(s1.getId(), subjectRun);
        assertFalse(subjectRunEnrollment.isAssessmentGiven()); // by default assessment is not given

        sut.giveAssessmentToStudent(t1, s1, subjectRunEnrollment); // act

        assertTrue(subjectRunEnrollment.isAssessmentGiven()); // assessment was given
        assertEquals(subjectRunEnrollment.getAssessmentGiver(), t1); // assessment was given by correct teacher
    }

    @Test
    public void test_teacherThatIsNotTutorOfSubjectParallelGivesAssessment_throwsAssessmentException() {
        subjectRunService.addTutor(this.subjectRun, t1);
        subjectRunService.addTutor(this.subjectRun, t2);
        subjectRunService.addTutorialParallel(this.subjectRun, tutorialParallelService.createTutorialParallel(
                "MON",
                LocalTime.now(),
                t2
        ));
        SubjectRunEnrollment subjectRunEnrollment = studentService.enrollSubject(s1.getId(), subjectRun);
        assertFalse(subjectRunEnrollment.isAssessmentGiven()); // by default assessment is not given

        assertThrows(TeacherCantGiveAssessment.class, () -> sut.giveAssessmentToStudent(t1, s1, subjectRunEnrollment));

        assertFalse(subjectRunEnrollment.isAssessmentGiven()); // assessment was not given
        assertNull(subjectRunEnrollment.getAssessmentGiver()); // assessment giver is null
    }
}
