package cz.minikos.service;

import cz.minikos.dao.SemesterDao;
import cz.minikos.environment.Generator;
import cz.minikos.exception.EnrollmentException;
import cz.minikos.exception.ExamTermDueException;
import cz.minikos.model.*;
import cz.minikos.util.Constants;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class StudentServiceTest {
    @Autowired
    StudentService sut;
    @Mock
    SemesterDao semesterDao;
    @PersistenceContext
    private EntityManager em;
    @Test
    public void enrollExamTerm() {
        Student s = Generator.generateStudent();
        ExamTerm examTerm = new ExamTerm();
        SubjectRun subjectRun = Generator.generateSubjectRun();
        subjectRun.setSemester(Generator.getcurrentSemester());

        em.persist(subjectRun);

        SubjectRunEnrollment subjectRunEnrollment = sut.enrollSubject(s, subjectRun);
        subjectRunEnrollment.setAssessmentGiven(true);
        em.merge(subjectRunEnrollment);

        examTerm.setSubjectRun(subjectRun);
        examTerm.setDate(LocalDateTime.of(2000, 2, 2, 2, 2, 2));
        ExamEnrollment examEnrollment = sut.enrollExamTerm(s, examTerm);

        assertEquals(s.getExamEnrollments().get(0), examEnrollment);

    }

    @Test
    public void cancelExamTerm_succeeds() {
        Student s = Generator.generateStudent();
        ExamTerm examTerm = new ExamTerm();
        SubjectRun subjectRun = Generator.generateSubjectRun();
        subjectRun.setSemester(Generator.getcurrentSemester());
        Semester currSemester = new Semester();
        currSemester.setName(Constants.CURRENT_SEMESTER_NAME);
        when(semesterDao.find(Constants.CURRENT_SEMESTER_NAME)).thenReturn(currSemester);
        em.persist(currSemester);
        subjectRun.setSemester(currSemester);
        em.persist(subjectRun);

        SubjectRunEnrollment subjectRunEnrollment = sut.enrollSubject(s, subjectRun);
        subjectRunEnrollment.setAssessmentGiven(true);
        em.merge(subjectRunEnrollment);

        examTerm.setSubjectRun(subjectRun);
        examTerm.setDate(LocalDateTime.of(2030, 2, 2, 2, 2, 2));
        ExamEnrollment examEnrollment = sut.enrollExamTerm(s, examTerm);

        assertEquals(s.getExamEnrollments().get(0), examEnrollment);
        assertEquals(1, s.getExamEnrollments().size());

        sut.cancelExamEnrollment(s, examTerm);

        assertEquals(0, s.getExamEnrollments().size());

    }

    @Test
    public void cancelExamTerm_throwsExamTermDueException() {
        Student s = Generator.generateStudent();
        ExamTerm examTerm = new ExamTerm();
        SubjectRun subjectRun = Generator.generateSubjectRun();
        subjectRun.setSemester(Generator.getcurrentSemester());

        em.persist(subjectRun);

        SubjectRunEnrollment subjectRunEnrollment = sut.enrollSubject(s, subjectRun);
        subjectRunEnrollment.setAssessmentGiven(true);
        em.merge(subjectRunEnrollment);

        examTerm.setSubjectRun(subjectRun);
        examTerm.setDate(LocalDateTime.of(2010, 2, 2, 2, 2, 2));
        ExamEnrollment examEnrollment = sut.enrollExamTerm(s, examTerm);

        assertEquals(s.getExamEnrollments().get(0), examEnrollment);
        assertEquals(1, s.getExamEnrollments().size());

        assertThrows(ExamTermDueException.class, () -> sut.cancelExamEnrollment(s, examTerm));

        assertEquals(1, s.getExamEnrollments().size());

    }

}
