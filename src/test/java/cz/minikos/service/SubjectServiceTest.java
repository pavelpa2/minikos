package cz.minikos.service;

import cz.minikos.dao.SubjectDao;
import cz.minikos.model.Subject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class SubjectServiceTest {
    @Autowired
    SubjectService sut;
    @Autowired
    SubjectDao dao;

    @Test
    public void createSubject() {
        String subjectName = "abc's of linear algebra";
        String subjectCode = "abc";
        Subject s = sut.createSubject(subjectName, subjectCode);
        assertEquals(s, dao.find(subjectName, subjectCode));
    }
}
