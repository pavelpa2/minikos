package cz.minikos.environment;

import cz.minikos.model.*;
import cz.minikos.util.Constants;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class Generator {
    private static final Random RAND = new Random();

    public static int randomInt() {
        return RAND.nextInt();
    }

    public static Teacher generateTeacher() {
        Teacher teacher = new Teacher();
        teacher.setPassword("abc");
        teacher.setUsername("UsernameTeacher"+randomInt());
        teacher.setName("Teacher " + randomInt());
        return teacher;
    }

    public static Student generateStudent() {
        Student student = new Student();
        student.setPassword("abc");
        student.setUsername("UsernameStudent"+randomInt());
        student.setName("Student " + randomInt());
        return student;
    }

    public static Student generateStudent(List<SubjectRunEnrollment> subjectRunEnrollmentList) {
        Student student = new Student();
        student.setName("Student " + randomInt());
        student.setPassword("abc");
        student.setSubjectRunEnrollments(subjectRunEnrollmentList);
        return student;
    }

    public static SubjectRun generateSubjectRun() {
        SubjectRun subjectRun = new SubjectRun();
        Subject s = new Subject();
        s.setName("name"+randomInt());
        s.setCode("b"+randomInt());
        subjectRun.setSubject(s);
        subjectRun.setCredits(randomInt());
        subjectRun.setSemester(generateSemester());
        return subjectRun;
    }

    public static SubjectRunEnrollment generateSubjectRunEnrollment(TutorialParallel tutorialParallel, SubjectRun subjectRun) {
        SubjectRunEnrollment subjectRunEnrollment = new SubjectRunEnrollment();
        subjectRunEnrollment.setSubjectRun(subjectRun);
        subjectRunEnrollment.setTutorialParallel(tutorialParallel);
        return subjectRunEnrollment;
    }

    public static Semester generateSemester() {
        Semester semester = new Semester();
        semester.setName("b"+randomInt());
        return semester;
    }

    public static Semester getcurrentSemester() {
        Semester semester = new Semester();
        semester.setName(Constants.CURRENT_SEMESTER_NAME);
        return semester;
    }

//    public static List<SubjectRunEnrollment> generateListOfSubjectRunEnrollments(SubjectRun subjectRun) {
//        SubjectRunEnrollment subjectRunEnrollment = new SubjectRunEnrollment();
//        subjectRunEnrollment.setSubjectRun(subjectRun);
//
//        List<SubjectRunEnrollment> subjectRunEnrollmentList = new ArrayList<>();
//        subjectRunEnrollmentList.add(subjectRunEnrollment);
//
//        return subjectRunEnrollmentList;
//    }

    public static TutorialParallel generateTutorialParallel() {
        TutorialParallel tutorialParallel = new TutorialParallel();
        tutorialParallel.setDay("MON");
        tutorialParallel.setTime(LocalTime.of(randomInt(), 0));
        return tutorialParallel;
    }

    public static TutorialParallel generateTutorialParallel(Teacher teacher) {
        TutorialParallel tutorialParallel = new TutorialParallel();
        tutorialParallel.setDay("MON");
        tutorialParallel.setTime(LocalTime.of(10, 1, 2));
        tutorialParallel.setTutor(teacher);
        return tutorialParallel;
    }

}
