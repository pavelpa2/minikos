<h3>Školní informační systém</h3>
V tomto projektu jsem se naučil pracovat s frameworkem Spring Boot a psát backend s využitím vícevrstevnaté architektury. 
<br>

<h4>Funkční uživatelé</h4>

- Student
- Teacher
- Administrátor

<h4>Funkce</h4>

- Zapisování na běh předmětu
- Student se může zapsat nejvýše na jeden běh předmětu konkrétního předmětu v jednom semestru
- Student se může zapsat na běh předmět maximálně dvakrát za studium
- Student potřebuje zápočet jako jedna z podmínek pro úspešné ukončení předmětu
- Zapisování na zkoušky
- Student se může zapsat na zkoušku pouze pokud mu byl udělen zápočet
- Student se může zapsat na zkoušku maximálně třikrát za jeden běh předmětu
- Potřetí se student může zapsat pouze, pokud využije žolíka
- Student se může odepsat ze zkoušky, ale pouze pokud je před termínem odpisu zkoušky (což je termín zkoušky)
- Student by neměl být schopen se zapsat na zkoušku, jestliže úspěšně absolvoval předchozí zkoušku (Tedy dostal alespoň E) ze stejného předmětu
- Známkování studenta
- Zápočet může udělit jedině cvičící studenta
- Známku ze zkoušky může udělit pouze přednášející, který vypsal termín zkoušky
- Známku ze zkoušky může být udělena pouze pokud student obdržel zápočet
- Funkce administrátora
- CRUD entit

<h4>Entity diagram</h4>
![Entity diagram](minikos_uml.png "Entity diagram")

